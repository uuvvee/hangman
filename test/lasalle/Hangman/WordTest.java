package lasalle.Hangman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class WordTest {

    private Random random = new Random();
    private String guesses[] = {
            "reddit", "facebook", "java", "assignment",
            "game", "hello", "islam", "religion", "internet", "face"};
    private char[] randomWordToGuess;
    private int ammountOfGuesses;
    int randomNumber = random.nextInt(guesses.length);

    @BeforeEach
    void setUp() {
        randomWordToGuess = guesses[randomNumber].toCharArray(); // java -> j,a,v,a
        ammountOfGuesses = randomWordToGuess.length; //total tries to guess a word.
    }

    @Test
    void getWordLenght() {
        assertEquals(ammountOfGuesses,randomWordToGuess.length);
    }

    @Test
    void getWord() {
        assertEquals(guesses[randomNumber],new String(randomWordToGuess));
    }

    @Test
    void checkChar() {
        //assertEquals(randomWordToGuess[1], guesses[1]);
    }
}