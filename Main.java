package lasalle;

import lasalle.Hangman.Game;
import lasalle.Hangman.*;

public class Main {
    public static void main(String[] args) {
        Game game = new Game();
        boolean testMode = false;
        game.start(testMode);
    }

}